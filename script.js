// console.log(`Congrats, batch 243!`);

// [SECTION] Document Object Model (DOM)
// allows us to access or modify the properties of an HTML element in a webpage
// it is standard on how to get, change, add or delete HTML elements
// we will focus on use of DOM in managing forms.

// For selecting HTML elements, we will be using document.querySelector
// Syntax: document.querySelector("html element")
// the querySelector function takes a string input that is formatted like a css selector when applying the styles

const txtFirstName = document.querySelector("#txt-first-name");
console.log(txtFirstName);

const txtLastName = document.querySelector("#txt-last-name");
console.log(txtLastName);

const name = document.querySelectorAll(".name");
console.log(name);

const span = document.querySelectorAll("span");
console.log(span);

const text = document.querySelectorAll("input[type]");
console.log(text);

// const div = document.querySelectorAll(".first-div > span");
// console.log(div);

// [SECTION] EVENT LISTENERS
// whenever a user interacts with a webpage, this action is considered as an event
// working with events is a large part of creating interactivity in a webpage
// specific functions that perform an action

//The function use is "addEventListener", it takes 2 arguments
// first argument a string identifying an event
// second argument, function that the listener will trigger once the "specified event" is triggered

// txtFirstName.addEventListener("keyup", (event) => {
//   console.log(event.target.value);
// });

const spanFullName = document.querySelector("#name-value");
console.log(spanFullName);
const fullName = () => {
  spanFullName.innerHTML = `${txtFirstName.value} ${txtLastName.value}`;
};

txtFirstName.addEventListener("keyup", fullName);
txtLastName.addEventListener("keyup", fullName);

const nameColor = document.querySelector("#colors");
console.log(nameColor);
console.log(nameColor.value);

const changeNameColor = () => {
  document.getElementById("name-value").style.color = `${nameColor.value}`;
};

nameColor.addEventListener("change", changeNameColor);
